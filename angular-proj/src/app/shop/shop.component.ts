import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

class Product{
  Id:number;
  Name:string;
  Cost:number;
}

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css']
})
export class ShopComponent implements OnInit {
  heroes = ['Windstorm', 'Bombasto', 'Magneta', 'Tornado'];
  prod:Product;
  prodArr:Product[];
  @ViewChild('prodId',{static:false}) prodId;
  @ViewChild('prodName',{static:false}) prodName;
  @ViewChild('cost',{static:false}) cost;

  constructor() {
    this.prod = new Product();
    this.prodArr = new Array();
   }

  ngOnInit() {
  }

  AddtoCart(prod){
    this.prod = new Product();
    let msgs = '';
    if(this.prodId.nativeElement.value == '')
      msgs+='Number is required\n';
    if(isNaN(this.prodId.nativeElement.value))
      msgs+='Number is invalid\n';
    if(this.prodName.nativeElement.value == '')
      msgs+='Name is required\n';
    if(this.cost.nativeElement.value == '')
      msgs+='Cost is required\n';
    if(isNaN(this.cost.nativeElement.value))
      msgs+='Cost is invalid\n';

    if(msgs==''){
      this.prod.Id = this.prodId.nativeElement.value;
      this.prod.Name = this.prodName.nativeElement.value;
      this.prod.Cost = this.cost.nativeElement.value;
      this.prodArr.push(this.prod);
    }
    else alert(msgs);
  }
}
