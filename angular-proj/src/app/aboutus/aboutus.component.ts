import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-aboutus',
  templateUrl: './aboutus.component.html',
  styleUrls: ['./aboutus.component.css']
})
export class AboutusComponent implements OnInit {
  prodOne:boolean=true;
  prodTwo:boolean=false;
  prodThree:boolean=false;

  constructor() { }

  ngOnInit() {
  }

  toggle(divId){
    if(divId=='PRODONE'){
      this.prodOne=true;
      this.prodTwo=this.prodThree=false;
    }
    else if(divId=='PRODTWO'){
      this.prodOne=false;
      this.prodTwo=true;this.prodThree=false;
    }
    else if(divId=='PRODTHREE'){
      this.prodOne=false;
      this.prodTwo=false;this.prodThree=true;
    } 
  }

}
